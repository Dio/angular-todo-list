#BUILD
FROM node:lts-alpine

RUN mkdir -p /app
WORKDIR /app
COPY package*.json /app/

RUN npm install
COPY . /app/
EXPOSE 4200

# Serve the app
CMD ["npm", "start"]